import React from 'react';
import ReactDOM from 'react-dom';

import { Posts } from './routes';

/**
 * Bootstrap for Application.
 */
window.onload = () => {
    ReactDOM.render(<Posts />, document.getElementById('app'));
}
