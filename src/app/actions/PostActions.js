import { Alt } from '../lib';
import wpcom from 'wpcom';

/**
 *
 */
class PostActions {

    /**
     *
     */
    getAllPosts( ){
        let self = this;
        wpcom().site( 'www.fjakkarin.com' ).postsList({ number: 15}, (err, data) => {
                if (err) {
                    self.onError( err );
                }
                // Only show the posts which are in the BitBucket category.
                let relevant = data.posts.filter( (post) => {
                        if(post.categories.BitBucket){
                            return post;
                        }
                    }
                );
                self.updatePosts( relevant );
            }
        );
        self.onWait();
    }

    /**
     *
     */
    updatePosts( posts ){
        return posts;
    }

    /**
     *
     */
    onWait(){
        return [];
    }

    /**
     *
     */
    onError( errorMessage ){
        return errorMessage;
    }
}

export default Alt.createActions(PostActions);
