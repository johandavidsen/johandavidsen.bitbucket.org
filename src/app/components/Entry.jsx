import React from 'react';
import createFragment from 'react-addons-create-fragment';

/**
 *
 */
export default class Entry extends React.Component {

    /**
     *
     */
    constructor( props ){
        super( props );

        this.state = {
            title: props.title,
            tags: Object.keys(props.tags),
            date: new Date(props.date),
            content: props.content,
            author: props.author
        }

    }

    /**
     *
     */
    render(){
        // The classes are dependant on the tags assigned to the posts.
        let classes = "";
        this.state.tags.map( (tag) => {
                classes += " " + tag.toLocaleLowerCase()
            }
        );

        // @todo: fix dangerouslySetInnerHTML
        return(
            <div className={"card card-block " + classes }>
                <h4 className="card-title">{this.state.title}</h4>
                <p className="card-text" dangerouslySetInnerHTML={{__html:this.state.content }}></p>
                <div className="media">
                    <a className="media-left" href={this.state.author.public_URL}>
                        <img className="media-object" src={this.state.author.avatar_URL} alt="Generic placeholder image" height="45px" width="45px" />
                    </a>
                    <div className="media-body">
                        <h6 className="media-heading"><a  class="card-signature" href={"mailto:" + this.state.author.email}>{this.state.author.name}</a> <small class="text-muted"><a href={this.state.author.public_URL}>@johandavidsen</a></small></h6>
                        <p><small className="text-muted">on { this.state.date.getDate() + "." + this.state.date.getMonth() + "." + this.state.date.getFullYear()} from Copenhagen, Denmark</small></p>
                    </div>
                </div>
            </div>
        );
    }
}
