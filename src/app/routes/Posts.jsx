import React from 'react';

import { Entry } from '../components';

import { PostActions } from '../actions';
import { PostStore } from '../stores';

/**
 *
 */
 export default class Posts extends React.Component {

    /**
     *
     */
    constructor( props ){
        super( props );

        this.state = PostStore.getState();

        this._onChange = this._onChange.bind(this);
    }

    /**
     *
     */
    componentDidMount(){
        PostStore.listen(this._onChange);
        PostActions.getAllPosts();
    }

    /**
     *
     */
    componentWillUnmount() {
        PostStore.unlisten(this._onChange);
    }

    /**
     *
     */
    _onChange( posts ){
        this.setState( posts );
    }

    /**
     *
     */
    render(){
    
        return(
            <div>
                {this.state.posts.map((post) => {
                    return (
                        <Entry key={post.global_ID} title={post.title} tags={post.tags} date={post.date} content={post.content} author={post.author} meta="" />
                    );
                })}
            </div>
        );
    }
}
