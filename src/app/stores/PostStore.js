import { Alt } from '../lib';

import { PostActions } from '../actions';

/**
 *
 */
class PostStore {

    /**
     *
     */
    constructor(){
        var self = this;
        this.bindListeners({
            setPosts: PostActions.updatePosts,
            onWait: PostActions.onWait
        });
        this.on('init', function(){
            self.posts = [];
        });

    }

    /**
     *
     */
    setPosts( posts ){
        this.posts = posts;
    }

    onWait( posts ){
        this.posts = posts;
    }
}

export default Alt.createStore(PostStore, "PostStore");
